package com.webciptan.calculator;

import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.*;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText numLeft;
    EditText numRight;

    Button butPlus;
    Button butMinus;
    Button butMulti;
    Button butDiv;

    TextView resultTextView;
    String msg = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {


           super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        // находим элементы
        numLeft = (EditText) findViewById(R.id.numLeft);
        numRight = (EditText) findViewById(R.id.numRight);

        butPlus = (Button) findViewById(R.id.butPlus);
        butMinus = (Button) findViewById(R.id.butMinus);
        butMulti = (Button) findViewById(R.id.butMulti);
        butDiv = (Button) findViewById(R.id.butDiv);

        resultTextView = (TextView) findViewById(R.id.resultTextView);

        // прописываем обработчик
        butPlus.setOnClickListener(this);
        butMinus.setOnClickListener(this);
        butMulti.setOnClickListener(this);
        butDiv.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub

        float result = 0;
        String s;
        String error = "";

        if(numLeft.getText().toString().isEmpty()||numRight.getText().toString().isEmpty())
        {
            return;
        }

        float numleft = Float.parseFloat(numLeft.getText().toString());
        float numright = Float.parseFloat(numRight.getText().toString());


        switch (v.getId()) {
            case R.id.butPlus:
                result = numleft + numright;
                break;
            case R.id.butMinus:
                result = numleft - numright;
                break;
            case R.id.butMulti:
                result = numleft * numright;
                break;
            case R.id.butDiv:
                if(numright == 0)
                {

                    error = "You can not divide by zero";
                }
                else
                {
                    result = numleft / numright;
                }


                break;
            default:
                break;
        }
        if(!error.isEmpty())
        {
           s = error;
        }
        else if(result == (int)result)
        {
            s = (int)result+"";
        }
        else
        {
            s = result+"";
        }
        resultTextView.setText(s);

    }
}
